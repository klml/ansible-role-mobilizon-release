ansible role to install mobilizon based on [//docs.joinmobilizon.org/administration/install/release/](https://docs.joinmobilizon.org/administration/install/release/)

[/rgenoud/ansible-role-mobilizon](https://framagit.org/rgenoud/ansible-role-mobilizon) is alternative ansible role, but based on [Source installer](https://docs.joinmobilizon.org/administration/install/source/).

tested on [console.hetzner.cloud](https://console.hetzner.cloud).

## Playbook

```
- name: Mobilizon
  hosts: all
  remote_user: root
  vars:
    instance_name: Mobilizon/test
    instance_description: "Mobilizon for fancytown"
    admin_email: contact@example.org
    email_server: localhost
    email_hostname: localhost
    email_port: 25
    email_username: nil

  roles:
  - mobilizon-release

```


* if you dont use `email_server: localhost` set `email_password`.
* dbpass is created automatically in `tasks/configuration.yaml` or overwrite it.

```
ansible-playbook playbook.yaml  -e email_password=nil -e dbpass=xxxxxxxxxxx -i inventory.ini
```

## usage

* https://mobilizemuc.org